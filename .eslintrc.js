module.exports = {
	env: {
		browser: true
	},
	parser: 'babel-eslint',
	extends: ['airbnb', 'airbnb/hooks', 'prettier'],
	rules: {
		'react/jsx-filename-extension': 0,
		'react/jsx-indent': [2, 'tab'],
		'react/jsx-indent-props': [2, 'tab'],
		'react/prop-types': 0
	}
}
