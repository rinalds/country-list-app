import React from 'react'
import styled from 'styled-components'

import CountryTable from './modules/CountryTable/CountryTable.container'
import CountryModal from './modules/CountryModal/CountryModal.container'

const App = ({ className }) => (
	<div className={className}>
		<CountryTable />
		<CountryModal />
	</div>
)

const StyledApp = styled(App)`
	max-width: 960px;
	width: 100%;
	margin: 0 auto;
	padding: 20px 0;

	@media only screen and (max-width: 600px) {
		padding: 0;
		margin: 0;
	}
`

export default StyledApp
