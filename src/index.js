import React from 'react'
import ReactDOM from 'react-dom'
import { ApolloProvider } from 'react-apollo'
import { InMemoryCache } from 'apollo-cache-inmemory'
import ApolloClient from 'apollo-boost'
import { createGlobalStyle } from 'styled-components'

import { resolvers, typeDefs } from './graphql/resolvers'

import App from './App'

const cache = new InMemoryCache()

const client = new ApolloClient({
	uri: 'https://countries.trevorblades.com/countries',
	cache,
	typeDefs,
	resolvers
})

client.writeData({
	data: {
		activeCountry: null
	}
})

const GlobalStyle = createGlobalStyle`
	html,
	body {
		height: 100%;
	}

	body {
		margin: 0;
		font-family: 'Baloo Thambi 2', cursive;
		font-weight: 400;
		background: #f0f5ff;
	}

	button {
		font-family: 'Baloo Thambi 2', cursive;
		font-weight: 400;
	}

	.mobile-only {
		display: none;
	}
`

const el = document.querySelector('#app')

ReactDOM.render(
	<ApolloProvider client={client}>
		<GlobalStyle />
		<App />
	</ApolloProvider>,
	el
)
