import { gql } from 'apollo-boost'

export const typeDefs = gql`
	extend type Mutation {
		SetActiveCountry(code: String): String
	}
`

const GET_ACTIVE_COUNTRY = gql`
	{
		activeCountry @client
	}
`

export const resolvers = {
	Mutation: {
		setActiveCountry: (_root, { code }, { cache }) => {
			cache.writeQuery({
				query: GET_ACTIVE_COUNTRY,
				data: { activeCountry: code }
			})

			return null
		}
	}
}
