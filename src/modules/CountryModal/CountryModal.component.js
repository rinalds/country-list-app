import React from 'react'

import Modal from '../../components/Modal/Modal.component'

import { CountryDescription } from './CountryModal.style'

const CountryModal = ({ loading, country, open, onClose }) => {
	const reduceLanguages = languages =>
		languages.reduce((str, language) => `${str} ${language.name}`, '')

	return (
		<Modal
			open={open}
			loading={loading}
			title={country ? `${country.name} ${country.emoji}` : 'Loading country'}
			onClose={onClose}
		>
			{country && (
				<CountryDescription>
					<h1>{country.name}</h1>
					<h2>{country.native}</h2>
					<h3>{`Part of ${country.continent.name}`}</h3>

					<br />

					<h3>
						<span>Capital</span>
						{country.capital}
					</h3>
					<h3>
						<span>Code</span>
						{country.code}
					</h3>
					<h3>
						<span>Phone</span>
						{country.phone}
					</h3>
					<h3>
						<span>Currency</span>
						{country.currency}
					</h3>

					<h3>
						<span>{`Spoken Language${country.languages.length > 1 ? 's' : ''}`}</span>
						{`${reduceLanguages(country.languages)}`}
					</h3>
				</CountryDescription>
			)}
		</Modal>
	)
}

export default CountryModal
