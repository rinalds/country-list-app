import React from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { gql } from 'apollo-boost'

import ErrorMessage from '../../components/ErrorMessage'

import CountryModal from './CountryModal.component'

const GET_ACTIVE_COUNTRY = gql`
	{
		activeCountry @client
	}
`

const GET_COUNTRY = gql`
	query Country($code: ID!) {
		country(code: $code) {
			code
			name
			native
			phone
			capital
			currency
			languages {
				name
				native
				rtl
			}
			continent {
				code
				name
			}
			emoji
			emojiU
			states {
				name
			}
		}
	}
`

const SET_ACTIVE_COUNTRY = gql`
	mutation SetActiveCountry($code: String) {
		setActiveCountry(code: $code) @client
	}
`

const CountryTableContainer = () => {
	const [setActiveCountry] = useMutation(SET_ACTIVE_COUNTRY)

	const {
		data: { activeCountry }
	} = useQuery(GET_ACTIVE_COUNTRY)

	const { loading, data, error } = useQuery(GET_COUNTRY, {
		variables: { code: activeCountry },
		skip: !activeCountry
	})

	if (error) {
		return (
			<ErrorMessage>
				Something went wrong with fetching Country. Please refresh page to try again
			</ErrorMessage>
		)
	}

	return (
		<CountryModal
			loading={loading}
			open={activeCountry}
			country={data ? data.country : null}
			onClose={() => setActiveCountry({ variables: { code: null } })}
		/>
	)
}

export default CountryTableContainer
