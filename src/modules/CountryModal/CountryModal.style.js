/* eslint-disable import/prefer-default-export */
import styled from 'styled-components'

export const CountryDescription = styled.div`
	text-align: center;
	padding: 20px;
	h1,
	h2,
	h3 {
		margin: 0;
	}
	h2 {
		font-weight: 400;
		margin-top: -5px;
		color: #949494;
	}

	h3 {
		margin-bottom: 10px;
		span {
			display: block;
			font-weight: 400;
			color: #8e8e8e;
		}
	}
`
