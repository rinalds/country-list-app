import React from 'react'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { gql } from 'apollo-boost'

import Table from '../../components/Table/Table.component'
import ErrorMessage from '../../components/ErrorMessage'

const GET_COUNTRIES = gql`
	{
		countries {
			code
			name
			capital
			currency
		}
	}
`

const SET_ACTIVE_COUNTRY = gql`
	mutation SetActiveCountry($code: String) {
		setActiveCountry(code: $code) @client
	}
`

const CountryTableContainer = () => {
	const { loading, data, error } = useQuery(GET_COUNTRIES)
	const [setActiveCountry] = useMutation(SET_ACTIVE_COUNTRY)

	if (error) {
		return (
			<ErrorMessage>
				Something went wrong with fetching Country. Please refresh page to try again
			</ErrorMessage>
		)
	}

	return (
		<Table
			header={[
				{
					title: 'Name',
					field: 'name'
				},
				{
					title: 'Capital',
					field: 'capital'
				},
				{
					title: 'Currency',
					field: 'currency'
				}
			]}
			idField='code'
			loading={loading}
			items={data ? data.countries : []}
			onSelect={code => setActiveCountry({ variables: { code } })}
		/>
	)
}

export default CountryTableContainer
