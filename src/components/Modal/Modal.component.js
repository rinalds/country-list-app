/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useEffect } from 'react'

import withStyle, { Card, CardOptions, CloseIcon, Content, Title } from './Modal.style'
import Spinner from '../Spinner'

const Modal = ({ title, className, children, open, loading, onClose }) => {
	useEffect(() => {
		const onEscape = e => {
			if (e.key === 'Escape') {
				onClose()
			}
		}

		if (open) {
			document.addEventListener('keydown', onEscape)
		} else {
			document.removeEventListener('keydown', onEscape)
		}
	}, [open, onClose])

	const onOverlayClick = e => {
		if (e.target === e.currentTarget) {
			onClose()
		}
	}

	return (
		<div className={className} onClick={onOverlayClick}>
			<Card>
				<CardOptions>
					<Title>{title}</Title>
					<CloseIcon onClick={onClose}>&#10005;</CloseIcon>
				</CardOptions>
				{loading ? <Spinner /> : <Content>{children}</Content>}
			</Card>
		</div>
	)
}

export default withStyle(Modal)
