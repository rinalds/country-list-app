import styled, { css } from 'styled-components'

export default Component => styled(Component)`
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	background: #0000007d;
	visibility: hidden;
	transition: 0.3s all;
	opacity: 0;

	${({ open }) =>
		open &&
		css`
			display: flex;
			visibility: visible;
			opacity: 1;
		`}
`

export const Card = styled.div`
	width: 100%;
	max-width: 400px;
	background: #fff;
	border-radius: 4px;
	height: 100%;
	max-height: 570px;
	display: flex;
	flex-direction: column;

	@media only screen and (max-width: 600px) {
		max-width: 100%;
		max-height: 100%;
		border-radius: 0;
	}
`

export const CardOptions = styled.div`
	padding: 0;
	background: #eaeaea;
	border-radius: 4px 4px 0 0;
	position: relative;

	@media only screen and (max-width: 600px) {
		border-radius: 0;
	}
`

export const CloseIcon = styled.button`
	background: transparent;
	border: 0;
	outline: none;
	color: black;
	font-size: 20px;
	padding: 10px;
	cursor: pointer;
	position: absolute;
	right: 0;
	top: 0;
	height: 100%;
	width: 42px;
`

export const Title = styled.h2`
	margin: 0;
	font-size: 18px;
	text-align: center;
	padding: 10px;
`

export const Content = styled.div`
	flex: 1;
`
