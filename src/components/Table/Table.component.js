import React from 'react'

import Spinner from '../Spinner'

import withStyle, { Table } from './Table.style'

import Button from '../Link'

const SmartTable = ({ header, items, idField, onSelect, className, loading }) => {
	const renderHeader = () => (
		<thead>
			<tr>
				{header.map(({ title, field }) => (
					<th key={field}>{title}</th>
				))}
			</tr>
		</thead>
	)

	const renderItem = item => (
		<tr key={item[idField]}>
			{header.map(({ field }, index) => (
				<td key={field}>
					{index === 0 ? (
						<Button onClick={() => onSelect(item[idField])}>{item[field]}</Button>
					) : (
						item[field]
					)}
				</td>
			))}
		</tr>
	)

	const renderRows = () => {
		return items.map(renderItem)
	}

	return (
		<div className={className}>
			{loading && <Spinner />}
			<Table>
				{renderHeader()}
				<tbody>{renderRows()}</tbody>
			</Table>
		</div>
	)
}

export default withStyle(SmartTable)
