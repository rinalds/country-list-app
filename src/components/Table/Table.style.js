import styled, { css } from 'styled-components'

export default Component => styled(Component)`
	height: calc(100vh - 40px);
	overflow-y: auto;
	overflow-x: hidden;
	border: 1px solid #d4d4d4;
	border-radius: 4px;
	box-sizing: border-box;

	${({ loading }) =>
		loading &&
		css`
			height: 500px;
			background: white;
			position: relative;
		`}

	@media only screen and (max-width: 600px) {
		height: 100vh;
		border-radius: 0;
	}
`

export const Table = styled.table`
	width: 100%;
	border-collapse: collapse;
	background: white;
	border-style: hidden;
	position: relative;

	th,
	td {
		text-align: left;
		padding: 4px 8px;
		border: 1px solid #d4d4d4;
	}

	th {
		position: sticky;
		position: -webkit-sticky;
		background: white;
		top: 0;
		box-shadow: 0 1px 0 0 #d4d4d4;
		font-weight: bold;
	}

	tbody tr {
		transition: 0.2s all;

		&:hover {
			background: #f7f7f7;
		}
	}
`
