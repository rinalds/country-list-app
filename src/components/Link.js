import styled from 'styled-components'

const Link = styled.button`
	text-decoration: none;
	color: #425eef;

	border: 0;
	font-size: 16px;
	display: inline-block;
	padding: 0;
	background: transparent;
	cursor: pointer;
	text-align: left;

	&:hover {
		background: transparent;
		text-decoration: underline;
	}
`

export default Link
