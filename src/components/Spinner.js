import styled from 'styled-components'

export default styled.div`
	border: 10px solid #00000026;
	width: 20px;
	height: 20px;
	position: absolute;
	top: 50%;
	left: 50%;
	animation: rotate 1s infinite linear;
	margin-top: -15px;
	margin-left: -15px;

	@keyframes rotate {
		from {
			transform: rotate(0deg);
		}
		to {
			transform: rotate(360deg);
		}
	}
`
