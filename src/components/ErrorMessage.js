import styled from 'styled-components'

export default styled.div`
	box-sizing: border-box;
	border: 1px solid #bd0202;
	position: absolute;
	top: 50%;
	left: 50%;
	padding: 10px;
	border-radius: 5px;
	background: #f95858;
	color: #fff;
	width: 100%;
	max-width: 400px;
	text-align: center;
	margin-left: -200px;
	margin-top: -50px;

	@media only screen and (max-width: 600px) {
		width: calc(100% - 10px);
		margin-left: 0;
		left: 5px;
	}
`
